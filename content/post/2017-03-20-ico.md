---
title: ICO
subtitle: 27 June 2017 - 1 June 2018
date: 2014-03-20
---

# First 5 Days
The first stage of the Eos ICO began on June 27th and ended on July 1st. During this period 200,000,000 tokens were distributed; this initial sale consisted of 20% of the total Eos tokens available.

# 350 Days After
For the next 350 days, 700,000,000 tokens (70% of the total 1,000,000,000) will be distributed. Every day 2,000,000 tokens will be made available within a 23 hour period. 

# Assigning Value
Unlike other tokens, Eos is not sold all at the same price. Everyday there is a 23 hour long sale where there is no cap on funds. At the end of that period, the remainng hour of the day is used to determine how many coins each investor receives.

The total amount of money raised each day becomes the official cost of the whole bundle of tokens being distributed in that period and tokens are distributed in proportion to the funds allocated by each individual.
In other words, each token isn't sold at set price but rather the value of each individual one depends on the demand for Eos during the specific sale in which it is being distributed.

# block.one's Share
The remaining 1,000,000 tokens (10%) will be reserved for block.one. These tokens cannot be traded or transfered on the Ethereum network and there purpose is to ensure that block.one has alligned interest with the people participating in the ICO.