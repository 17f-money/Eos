---
title: What is Eos?
date: 2017-11-10
---

Eos is a temporary token which is being distributed on the Ethereum blokchain and will more than likely not continue onto the main EOS blockchain, which is being developed by block.one and has yet to be released. As of now, Eos token is just a tool set up to raise funds for block.one to develop the software for the EOS blockchain and it is more than likely that that is all they were meant for.
The EOS Team has stated over and over on their website, in their white paper, and in their interviews that:

"The EOS Tokens do not have any rights, uses, purpose, attributes, functionalities or features, express or implied, including, without limitation, any uses, purpose, attributes, functionalities or features on the EOS Platform."

NOTE: Eos refers to the token. EOS refers to the blockchain.