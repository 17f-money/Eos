---
title: Incentive
subtitle: Why do people buy Eos tokens?
date: 2017-10-20
---

Seeing as how Eos token appears to have no practicle purpose on the EOS blockchain, it seems almost miraculous that the creators have managed to distribute any of the tokens at all. Afterall, why would any one spend money on a token that will not benefit them monetarily in the future?

The answer is computing resources. The creators have claimed that every person who holds Eos tokens will be given a percentage of the total computing power proportional to the amount of tokens they own. In other words if someone owns 1% of the total Eos tokens and there are 100 CPUs supporting the EOS blockchain then that someone essentially has the right to consume 1 CPU.
Considering how many Eos tokens have been distributed since the beginnig of the ICO (June 2017) this approach seems to be pretty effective.

# Problems (RED FLAG)
However effective and fair this approach seems to be, if you look a little closer you'll notice that the framework for the EOS blockchain has an uncomfotable loophope.

The creators have stated that the main blockchain will be released "under an open source software license" allowing third partie to modify it as they wish. The problem here is that these third parties can hypotheitcally also exlude the original funders, the people who bought tokens before the release.
Also, the white paper seems to almost confirm this by stating:

"Cryptographic tokens referred to in this white paper refer to cryptographic tokens on a launched blockchain that adopts the eos.io software. They do not refer to the ERC-20 compatible tokens being distributed on the Ethereum blockchain in connection with the EOS token distribution."

The coins which will be used on the blockchain will be released with the blockchain, so once the ICO is over the creators will no longer have any incentive to keep or protect the EOs token and those who hold them.

There is also one other problem but this one only applies to residents of the United States. Eos token is not sold in the USA because "of some of the logistical challenges associated with differing regulations in the many states." The creators continue there explanation by furthur confirming that the Eos tokens "are not designed for investment or speculative purposes and should not be considered as a type of investment."